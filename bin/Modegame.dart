import 'dart:io';
import 'dart:math';

import 'Hero.dart';
import 'game.dart';

class Modegame {
  var correct = false;
  var guess = 0;
  int num = 0;
  int numA = 0;
  int round = 0;

  void check() {
    print("Select Level mode : [ 1 : Level 1 , 2 : Level 2 , 3 : Level 3 ]");
    String level = stdin.readLineSync()!.toLowerCase();
    print("");
    print(r'''
 _____  ______  ___   _____  ______    _____   ___   __  __  _____
/  ___>|_    _|/   \ |  _  \|_    _|  /  ___\ /   \ |  \/  ||  ___|
\___  \  |  | /  ^  \|     /  |  |    |  \_ \/  ^  \|      ||  ___|
<_____/  |__| |__|__||__|__\  |__|    \_____/|__|__||_|\/|_||_____|
            
            ''');
    if (level == '1') {
      var rand = new Random();
      int number = rand.nextInt(100) + 1;

      while (!correct && round < 15) {
        print("");

        stdout.write("Please enter a number [ 1 - 100 ] ????" + "\n");
        stdout.write("Attack !!! >>> ");

        try {
          guess = int.parse(stdin.readLineSync()!);
        } on FormatException {
          print("That isn't a recognized number,try again.");
          continue;
        }

        if (guess < number) {
          print("Still not correct, You should guess [ More ].");
          num++;
        } else if (guess > number) {
          print("Still not correct, You should guess [ Less ].");
          num++;
        } else {
          print("");
          print(r'''     
                           _   _   _
 __    __   __   __  __   | | | | | |
|  |/\|  | |  | |  \|  |  | | | | | |
|        | |  | |      |  |_| |_| |_|
 \__/\__/  |__| |__|\__|  |_| |_| |_|    
            ''');
          correct = true;
          int num2 = (num + 1);
          print("Number of rounds [ $num2 ]");
          break;
        }
        round++;
        
        if (round == 15) {
          print(r'''       
  __ _  __ _ _ __ ___   ___  ___      ___  __   __ ___  _ __
 / _` |/ _` | '_ ` _ \ / _ \/ __|    / _ \ \ \ / // _ \|  __/
| (_| | (_| | | | | | |  __/\__ \   | (_) | \ V /   __/| |
 \__, |\__,_|_| |_| |_|\___||___/    \___/   \_/  \___||_| 
 |___/        
          
          ''');
        } else if (round == round) print("");
        print("Round = [$round]");
      }
    } else if (level == '2') {
      var rand = new Random();
      int number = rand.nextInt(300) + 1;

      while (!correct && round < 10) {
        print("");

        stdout.write("Please enter a number [ 1 - 150 ] ????" + "\n");
        stdout.write("Attack !!! >>> ");

        try {
          guess = int.parse(stdin.readLineSync()!);
        } on FormatException {
          print("That isn't a recognized number,try again.");
          continue;
        }

        if (guess < number) {
          print("Still not correct, You should guess [ More ].");
          num++;
        } else if (guess > number) {
          print("Still not correct, You should guess [ Less ].");
          num++;
        } else {
          print("");
          print(r'''     
                           _   _   _
 __    __   __   __  __   | | | | | |
|  |/\|  | |  | |  \|  |  | | | | | |
|        | |  | |      |  |_| |_| |_|
 \__/\__/  |__| |__|\__|  |_| |_| |_|    
            ''');
          correct = true;
          int num2 = (num + 1);
          print("Number of rounds [ $num2 ]");

          break;
        }
        round++;
        if (round == 10) {
          print(r'''       
  __ _  __ _ _ __ ___   ___  ___      ___  __   __ ___  _ __
 / _` |/ _` | '_ ` _ \ / _ \/ __|    / _ \ \ \ / // _ \|  __/
| (_| | (_| | | | | | |  __/\__ \   | (_) | \ V /   __/| |
 \__, |\__,_|_| |_| |_|\___||___/    \___/   \_/  \___||_| 
 |___/        
          
          ''');
        } else if (round == round) print("");
        print("Round = [$round]");
      }
    } else if (level == '3') {
      var rand = new Random();
      int number = rand.nextInt(500) + 1;

      while (!correct && round < 9) {
        print("");

        stdout.write("Please enter a number [ 1 - 250 ] ????" + "\n");
        stdout.write("Attack !!! >>> ");

        try {
          guess = int.parse(stdin.readLineSync()!);
        } on FormatException {
          print("That isn't a recognized number,try again.");
          continue;
        }

        if (guess < number) {
          print("Still not correct, You should guess [ More ].");
          num++;
        } else if (guess > number) {
          print("Still not correct, You should guess [ Less ].");
          num++;
        } else {
          print("");
          print(r'''     
                           _   _   _
 __    __   __   __  __   | | | | | |
|  |/\|  | |  | |  \|  |  | | | | | |
|        | |  | |      |  |_| |_| |_|
 \__/\__/  |__| |__|\__|  |_| |_| |_|    
            ''');
          correct = true;
          int num2 = (num + 1);
          print("Number of rounds [ $num2 ]");

          break;
        }
        round++;
        if (round == 9) {
          print(r'''       
  __ _  __ _ _ __ ___   ___  ___      ___  __   __ ___  _ __
 / _` |/ _` | '_ ` _ \ / _ \/ __|    / _ \ \ \ / // _ \|  __/
| (_| | (_| | | | | | |  __/\__ \   | (_) | \ V /   __/| |
 \__, |\__,_|_| |_| |_|\___||___/    \___/   \_/  \___||_| 
 |___/        
          
          ''');
        } else if (round == round) print("");
        print("Round = [$round]");
      }
    }
  }
}
