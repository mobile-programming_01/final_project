import 'dart:io';
import 'dart:math';

class Modegame2 {
  var correct = false;
  var guess = 0;
  int numA = 0;
  int round = 0;

  void check() {
    print("Please name your team.");
    print("");
    print("Team 1 : ");
    String team1 = stdin.readLineSync()!.toLowerCase();
    print("Team 2 : ");
    String team2 = stdin.readLineSync()!.toLowerCase();
    print("");
    print("Choose the number of rounds to play?");
    int rou = int.parse(stdin.readLineSync()!);

    print("");
    print("Team 1 : [ $team1 ] vs Team 2 : [ $team2 ]");
    print("");

    print(r'''
 _____  ______  ___   _____  ______    _____   ___   __  __  _____
/  ___>|_    _|/   \ |  _  \|_    _|  /  ___\ /   \ |  \/  ||  ___|
\___  \  |  | /  ^  \|     /  |  |    |  \_ \/  ^  \|      ||  ___|
<_____/  |__| |__|__||__|__\  |__|    \_____/|__|__||_|\/|_||_____|
        
           
                        ___  ___ _____               
           //           \  \/  //  ___>              \\       
          //             \    / \___  \               \\
         //               \__/  <_____/                \\
        //       _____  __   _____  __   __  ______     \\
        \\      |  ___||  | /  ___\|  |_|  ||_    _|    //
         \\     |  __| |  | |  \_ \|   _   |  |  |     //
          \\    |__|   |__| \_____/|__| |__|  |__|    //
           \\                                        //
              ''');

    var rand = new Random();
    int number = rand.nextInt(100) + 1;

    while(!correct && round < 10){
    print("");
    stdout.write("Please enter a number [ 1 - 100 ] ????"+"\n");
    stdout.write(" [ Team1 ] The number you enter is >>>>> ");
    try{
      guess = int.parse(stdin.readLineSync()!);
    }
    on FormatException{
      print("That isn't a recognized number,try again.");
      continue;
    }
    if(guess < number){
      print("Still not correct, You should guess [ More ].");
      numA++;
    }else if(guess > number){
      print("Still not correct, You should guess [ Less ].");
      numA++;
    }else{
          print("");
          print("[ You found it!!!!!!!! ]");
          correct = true;
          int numAA = (numA+1);
          print("Number of rounds [ $numAA ]");     
          break;      
        }  
        round++;
        if(round == rou){
        print(r'''       
  __ _  __ _ _ __ ___   ___  ___      ___  __   __ ___  _ __
 / _` |/ _` | '_ ` _ \ / _ \/ __|    / _ \ \ \ / // _ \|  __/
| (_| | (_| | | | | | |  __/\__ \   | (_) | \ V /   __/| |
 \__, |\__,_|_| |_| |_|\___||___/    \___/   \_/  \___||_| 
 |___/        
          
          ''');
        round=numA;
        break;
        }else if(round == round)
        print("");
        print("Round = [$round]");
  }
   int number2 = rand.nextInt(100) + 1 ;
      
      var guess2 = 0;
      int numB = 0;
      int round2 = 0;
      correct = false;
      print("");
      print("Team 2 : [ $team2 ]");
         
      while(!correct && round2 < rou){
      print("");
      stdout.write("Please enter a number [ 1 - 100 ] ????"+"\n");
      stdout.write("[ Team2 ] The number you enter is >>>>> ");
      try{
      guess2 = int.parse(stdin.readLineSync()!);
      }
      on FormatException{
      print("That isn't a recognized number,try again.");
      continue;
      }

      if(guess2 < number2){
        print("Still not correct, You should guess [ More ].");
        numB++;
      }else if(guess2 > number2){
        print("Still not correct, You should guess [ Less ].");
        numB++;
      }else{
        print("");
        print("[ You found it!!!!!!!! ]");
      correct = true;
      int numBB = (numB+1);
      print("Number of rounds [ $numBB ]");
      print("");
      break;
    }

      round2++;
      if(round2 == rou){
        print(r'''       
  __ _  __ _ _ __ ___   ___  ___      ___  __   __ ___  _ __
 / _` |/ _` | '_ ` _ \ / _ \/ __|    / _ \ \ \ / // _ \|  __/
| (_| | (_| | | | | | |  __/\__ \   | (_) | \ V /   __/| |
 \__, |\__,_|_| |_| |_|\___||___/    \___/   \_/  \___||_| 
 |___/        
          
          ''');
      round2=numB;
      break;
      }else if(round2 == round2)
        print("");
        print("Round = [$round2]");

      }
      int numAA = numA+1;
    int numBB=numB+1;

    if(numAA == rou+1){
        print("[ $team1 ] = Number of rounds [ GAME OVER ]");   
    }else{
        print("[ $team1 ] = Number of rounds [ $numAA ]");
    }   
    if(numBB == rou+1){        
        print("[ $team2 ] = Number of rounds [ GAME OVER ]");   
    }else{
        print("[ $team2 ] = Number of rounds [ $numBB ]");
    }        
     print("");
     if((numAA < numBB) || (numBB == rou && numAA != rou)){
        print(r'''     
                           _   _   _
 __    __   __   __  __   | | | | | |
|  |/\|  | |  | |  \|  |  | | | | | |
|        | |  | |      |  |_| |_| |_|
 \__/\__/  |__| |__|\__|  |_| |_| |_|    
            ''');
        
        print("[[[ CONGRATULSTIONS , You Win!!!!!! [ >>  $team1  << ]  ]]]");
     }else if((numBB < numAA) || (numBB != rou && numAA == rou)){
        print(r'''     
                           _   _   _
 __    __   __   __  __   | | | | | |
|  |/\|  | |  | |  \|  |  | | | | | |
|        | |  | |      |  |_| |_| |_|
 \__/\__/  |__| |__|\__|  |_| |_| |_|    
            ''');
        
        print("[[[ CONGRATULSTIONS , You Win!!!!!! [ >>  $team2  << ]  ]]]");
     }else if((numBB == rou && numAA == rou)||(numAA == numBB)){
        print(r'''
                                 _   _   _
 ____   _____  _____  __    __  | | | | | | 
|    \ |  _  \/  _  \|  |/\|  | | | | | | |
|  |  ||     /|  _  ||        | |_| |_| |_|
|____/ |__|__\\_/ \_/ \__/\__/  |_| |_| |_|
                
            ''');
        print("[ >> $team1 << ] DROW!!!!! [ >> $team2 << ]");
     }
  }
}
