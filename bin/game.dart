import 'dart:io';
import 'dart:math';
import 'Hero.dart';
import 'package:final_project/final_project.dart' as final_project;

import 'Modegame.dart';
import 'Modegame2.dart';

void main(List<String> arguments) {
  print(
      r'''
                  __  __   __     _,
                  \\  \\   / ___ '||  ___  ___ __  _  _   ___    
                   \\ /\\ / //_\) || // \)// \\ ||'||'|| //_\)   
                    \/  \/  \\__,_||_\\__,\\_//_||_||_||_\\__,                    
        

                                   _/|_ ___
                                    || // \\
                                    \|_\\_//  

                         __ _  __ _ _ __ ___   ___  ___
                        / _` |/ _` | '_ ` _ \ / _ \/ __|
                       | (_| | (_| | | | | | |  __/\__ \
                        \__, |\__,_|_| |_| |_|\___||___/
                        |___/

        ''');

  print("Select game mode : [ 1 Player or 2 Players ]");
  String player = stdin.readLineSync()!.toLowerCase();
  if (player == "1") {
    print("");
    print("!!!!!!!![ You choose 1 player game mode ]!!!!!!!!!!!");
    print(
        "You should choose a hero to play? [ 1 : Warrior ] [ 2 : Wizard ] [ 3 : Mage ] [ 4 : Hunter ] ");
    String hero = stdin.readLineSync()!.toLowerCase();
    if (hero == '1') {
      Hero warrior = Hero();
      warrior.Warrior();
    } else if (hero == '2') {
      Hero wizard = Hero();
      wizard.Wizard();
    } else if (hero == '3') {
      Hero mage = Hero();
      mage.Mage();
    } else if (hero == '4') {
      Hero hunter = Hero();
      hunter.Hunter();
    }

    Modegame modegame = Modegame();
    modegame.check();
  } else if (player == "2") {
    print("");
    print("!!!!!!!![ You choose 2 player game mode ]!!!!!!!!!!!");
    print("");

    int num = 0;
    do {
      print(
          "You should choose a hero to play? [ 1 : Warrior ] [ 2 : Wizard ] [ 3 : Mage ] [ 4 : Hunter ]");
      print("Team 1 : Choose Hero?");
      String hero1 = stdin.readLineSync()!.toLowerCase();

      print("Team 2 : Choose Hero?");
      String hero2 = stdin.readLineSync()!.toLowerCase();

      Hero warrior = Hero();
      Hero wizard = Hero();
      Hero mage = Hero();
      Hero hunter = Hero();

      if (hero1 == "1" && hero2 == "2") {
        warrior.Warrior();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       ''');
        wizard.Wizard();
        num = 1;
      } else if (hero1 == "1" && hero2 == "3") {
        warrior.Warrior();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        mage.Mage();
        num = 1;
      } else if (hero1 == "1" && hero2 == "4") {
        warrior.Warrior();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        hunter.Hunter();
        num = 1;
      } else if (hero1 == "2" && hero2 == "1") {
        wizard.Wizard();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        warrior.Warrior();
        num = 1;
      } else if (hero1 == "2" && hero2 == "3") {
        wizard.Wizard();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        mage.Mage();
        num = 1;
      } else if (hero1 == "2" && hero2 == "4") {
        wizard.Wizard();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        hunter.Hunter();
        num = 1;
      } else if (hero1 == "3" && hero2 == "1") {
        mage.Mage();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        warrior.Warrior();
        ;
        num = 1;
      } else if (hero1 == "3" && hero2 == "2") {
        mage.Mage();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        wizard.Wizard();
        num = 1;
      } else if (hero1 == "3" && hero2 == "4") {
        mage.Mage();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        hunter.Hunter();
        num = 1;
      } else if (hero1 == "4" && hero2 == "1") {
        hunter.Hunter();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        warrior.Warrior();
        num = 1;
      } else if (hero1 == "4" && hero2 == "2") {
        hunter.Hunter();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        wizard.Wizard();
        num = 1;
      } else if (hero1 == "4" && hero2 == "3") {
        hunter.Hunter();
        print(
            r'''       ___  ___ _____               
       \  \/  //  ___>                     
        \    / \___  \               
         \__/  <_____/ 
       
       ''');
        mage.Mage();
        num = 1;
      } else if ((hero1 == "1" && hero2 == "1") ||
          (hero1 == "2" && hero2 == "2") ||
          (hero1 == "3" && hero2 == "3") ||
          (hero1 == "4" && hero2 == "4")) {
        print("Choose again");
        num = 0;
      } else {
        print("Choose again");
        num = 0;
      }
    } while (num != 1);

    Modegame2 game = Modegame2();
    game.check();
  }
}
